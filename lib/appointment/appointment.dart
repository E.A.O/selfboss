import 'package:flutter/material.dart';
import 'package:selfboss/custom_scaffold.dart';
import 'package:selfboss/screen_arguments.dart';

class Appointment extends StatefulWidget {
  @override
  _AppointmentState createState() => _AppointmentState();
}

class _AppointmentState extends State<Appointment> {
  @override
  Widget build(BuildContext context) {
    final ScreenArguments args = ModalRoute.of(context).settings.arguments;

    return CustomScaffold(
      appBar: AppBar(
        title: Text(args.routeName),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            }),
      ),
      body: Container(
        child: Column(
          children: [
            Text('this is the appointments page'),
            RaisedButton(
              onPressed: () {
                bool isNewRouteSameAsCurrent = false;
                Navigator.popUntil(context, (route) {
                  print(route.settings);
                  if (route.settings.name == "routeName") {
                    print('same route found');
                    isNewRouteSameAsCurrent = true;
                  }
                  return true;
                });
                print(isNewRouteSameAsCurrent);
              },
              child: Text('Click here'),
            )
          ],
        ),
      ),
    );
  }
}
