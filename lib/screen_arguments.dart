class ScreenArguments {
  final String title;
  final String routeName;

  ScreenArguments(this.title, this.routeName);
}
