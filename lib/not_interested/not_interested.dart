import 'package:flutter/material.dart';
import 'package:selfboss/custom_scaffold.dart';
import 'package:selfboss/screen_arguments.dart';

class NotInterested extends StatefulWidget {
  @override
  _NotInterestedState createState() => _NotInterestedState();
}

class _NotInterestedState extends State<NotInterested> {
  @override
  Widget build(BuildContext context) {
    final ScreenArguments args = ModalRoute.of(context).settings.arguments;

    return CustomScaffold(
      appBar: AppBar(
        title: Text(args.routeName),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            }),
      ),
      body: Container(
        child: Text('this is the not interested page'),
      ),
    );
  }
}
