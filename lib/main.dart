import 'package:flutter/material.dart';
import 'package:selfboss/add_contact.dart';
import 'package:selfboss/appointment/appointment.dart';
import 'package:selfboss/contact/contact.dart';
import 'package:selfboss/customers/customers.dart';
import 'package:selfboss/demo_chart.dart';
import 'package:selfboss/drawer.dart';
import 'package:selfboss/drawerHeader.dart';
import 'package:selfboss/follow_up/follow_up.dart';
import 'package:selfboss/lead/lead.dart';
import 'package:selfboss/not_interested/not_interested.dart';
import 'package:selfboss/presentation/presentation.dart';

void main() {
  runApp(
    SelfBoss(),
  );
}

class SelfBoss extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Color.fromRGBO(66, 39, 90, 1),
      ),
      title: 'EAO Designs',
      initialRoute: '/',
      routes: {
        '/': (context) => SelfBossMain(),
        '/lead': (context) => Lead(),
        '/contact': (context) => Contact(),
        '/appointment': (context) => Appointment(),
        '/presentation': (context) => Presentation(),
        '/follow_up': (context) => FollowUp(),
        '/customers': (context) => Customers(),
        '/not_interested': (context) => NotInterested(),
      },
    );
  }
}

class SelfBossMain extends StatefulWidget {
  @override
  _SelfBossMainState createState() => _SelfBossMainState();
}

class _SelfBossMainState extends State<SelfBossMain> {
  CustomDrawerHeader customDrawerHeader = CustomDrawerHeader();
  AddContact addContact = AddContact();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 2,
        title: Text('SelfBoss'),
        backgroundColor: Color.fromRGBO(66, 39, 90, 1),
      ),
      drawer: MainDrawer(),
      body: Container(
        decoration: BoxDecoration(),
        padding: EdgeInsets.all(5.0),
        child: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 2,
                  child: Card(
                    elevation: 3.0,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8.0, 8.0, 0.0, 8.0),
                      child: SimpleBarChart.withSampleData(),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 2,
                  child: Card(
                    elevation: 3.0,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8.0, 8.0, 0.0, 8.0),
                      child: SimpleBarChart.withSampleData(),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          addContact.addContact(context);
        },
        tooltip: 'Add Contact',
        child: Icon(Icons.add),
        backgroundColor: Color.fromRGBO(66, 39, 90, 1),
      ),
    );
  }
}
