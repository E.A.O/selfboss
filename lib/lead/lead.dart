import 'package:flutter/material.dart';
import 'package:selfboss/custom_scaffold.dart';

import '../screen_arguments.dart';

class Lead extends StatefulWidget {
  @override
  _LeadState createState() => _LeadState();
}

class _LeadState extends State<Lead> {
  @override
  Widget build(BuildContext context) {
    final ScreenArguments args = ModalRoute.of(context).settings.arguments;
    return CustomScaffold(
      appBar: AppBar(
        title: Text(args.routeName),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            }),
      ),
      body: Container(
        child: Text('this is the lead page'),
      ),
    );
  }
}
