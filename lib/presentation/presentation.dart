import 'package:flutter/material.dart';
import 'package:selfboss/custom_scaffold.dart';
import 'package:selfboss/screen_arguments.dart';

class Presentation extends StatefulWidget {
  @override
  _PresentationState createState() => _PresentationState();
}

class _PresentationState extends State<Presentation> {
  @override
  Widget build(BuildContext context) {
    final ScreenArguments args = ModalRoute.of(context).settings.arguments;

    return CustomScaffold(
      appBar: AppBar(
        title: Text(args.routeName),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            }),
      ),
      body: Container(
        child: Text('this is the presentations page'),
      ),
    );
  }
}
