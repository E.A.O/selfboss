import 'package:flutter/material.dart';
import 'package:selfboss/drawerHeader.dart';
import 'package:selfboss/screen_arguments.dart';

class MainDrawer extends StatefulWidget {
  @override
  _MainDrawerState createState() => _MainDrawerState();
}

class _MainDrawerState extends State<MainDrawer> {
  CustomDrawerHeader customDrawerHeader = CustomDrawerHeader();
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          Container(height: 220, child: customDrawerHeader.drawerHeader),
          customListTile(
            title: 'Lead',
            routeName: 'lead',
            icon: Icon(Icons.input, size: 25),
          ),
          customListTile(
            title: 'Contact',
            routeName: 'contact',
            icon: Icon(Icons.contacts, size: 25),
          ),
          customListTile(
            title: 'Appointment',
            routeName: 'appointment',
            icon: Icon(Icons.timelapse, size: 25),
          ),
          customListTile(
            title: 'Presentation',
            routeName: 'presentation',
            icon: Icon(Icons.timeline, size: 25),
          ),
          customListTile(
            title: 'Follow Up',
            routeName: 'follow_up',
            icon: Icon(Icons.directions_walk, size: 25),
          ),
          customListTile(
            title: 'Customers',
            routeName: 'customers',
            icon: Icon(Icons.supervisor_account, size: 25),
          ),
          customListTile(
            title: 'Not Interested',
            routeName: 'not_interested',
            icon: Icon(Icons.rotate_90_degrees_ccw, size: 25),
          ),
        ],
      ),
    );
  }

  void navigateToPage(String routeName, String title) {
    bool isNewRouteSameAsCurrent = false;

    Navigator.popUntil(context, (route) {
      if (route.settings.name == '/$routeName') {
        isNewRouteSameAsCurrent = true;
      }
      return true;
    });
    Navigator.pop(context);
    if (!isNewRouteSameAsCurrent) {
      Navigator.pushNamed(context, '/$routeName',
          arguments: ScreenArguments(routeName, title));
    }
  }

  ListTile customListTile({String title, String routeName, Icon icon}) {
    return ListTile(
      title: Text(
        title,
        style: TextStyle(
          fontSize: 18.0,
          fontWeight: FontWeight.normal,
        ),
      ),
      leading: icon,
      onTap: () {
        navigateToPage(routeName, title);
      },
    );
  }
}
