import 'package:flutter/material.dart';

class CustomDrawerHeader {
  DrawerHeader _drawerHeaderBuilder() {
    return DrawerHeader(
      padding: EdgeInsets.zero,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
            Color.fromRGBO(66, 39, 90, 1),
            Color.fromRGBO(115, 75, 109, 1),
          ],
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.zero,
                child: FlatButton.icon(
                  onPressed: () {},
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  icon: Icon(
                    Icons.edit,
                    color: Colors.white,
                  ),
                  label: Text(
                    'Edit Profile',
                    style: TextStyle(color: Colors.white),
                  ),
                  // padding: EdgeInsets.zero,
                ),
                // width: double.infinity,
              ),
            ],
          ),
          Center(
            child: CircleAvatar(
              radius: 50.0,
              backgroundImage: AssetImage('images/logo.png'),
            ),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'The Pipeline',
                style: TextStyle(
                  fontFamily: 'Vollkron',
                  fontSize: 20.0,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  DrawerHeader get drawerHeader => _drawerHeaderBuilder();
}
