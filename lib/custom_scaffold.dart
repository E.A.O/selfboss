import 'package:flutter/material.dart';
import 'package:selfboss/drawer.dart';

class CustomScaffold extends StatelessWidget {
  final Widget body;
  final AppBar appBar;

  const CustomScaffold({Key key, this.body, this.appBar}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar,
      body: body,
      drawer: MainDrawer(),
    );
  }
}
